﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Unity.Collections;
using UnityEngine;

namespace Wander
{
    // Loads and parses data in the background. 
    // This one is faster in the background but the 'ToMesh' function of MeshArrays is slower on the main thread than MeshNativeArrays.
    public class WaitForArrayFromPlyAsync :CustomYieldInstruction
    {
        public override bool keepWaiting => !t.IsCompleted;
        public bool Succesful => t.IsCompletedSuccessfully;
        public MeshArrays Arrays => t.Result;

        public WaitForArrayFromPlyAsync( string ply, bool generateNormals, bool showWarnings, bool doUnsafe )
        {
            t = new Task<MeshArrays>( () => PLYUtils.CreateArraysFromPly( ply, generateNormals, showWarnings, doUnsafe ) );
            t.Start();
        }

        Task<MeshArrays> t;
    }

    // This one is slower than the above in the background but faster on the main thread. Resulting in less hickup.
    public class WaitForNatArrayFromPlyAsync :CustomYieldInstruction
    {
        public override bool keepWaiting
        {
            get
            {
                if ( t.IsCompleted )
                {
                    tskEnd?.Invoke( tskId.Value );
                    return false;
                }
                return true;
            }
        }
            
        public bool Succesful => t.IsCompletedSuccessfully;
        public MeshNativeArrays Arrays => t.Result;
        public int? BgtTaskId => tskId;

        public WaitForNatArrayFromPlyAsync( string ply, bool showWarnings, bool doUnsafe, Func<int> tskStart=null, Action<int> _tskEnd=null)
        {
            t = new Task<MeshNativeArrays>( () =>
            {
                var ext = Path.GetExtension( ply );
                string cacheName = Cache.GetCacheName( ply );
                var arr = MeshNativeArrays.FromCache( cacheName );
                if ( arr.vertices.Length > 0 )
                    return arr;
                arr = PLYUtils.CreateNativeArraysFromPly( ply, showWarnings, doUnsafe );
                arr.ToCache( cacheName );
                return arr;
            } );
            tskId  = tskStart?.Invoke();
            tskEnd = _tskEnd;
            t.Start();
        }

        public Mesh ToMesh( bool markNoLongerReadable ) 
        {
            return Arrays.ToMeshPositionNormalColor( markNoLongerReadable );
        }

        public Mesh ToMeshAndDisposeArrays( bool markNoLongerReadable )
        {
            Mesh m = Arrays.ToMeshPositionNormalColor( markNoLongerReadable );
            Dispose();
            return m;
        }

        public void Dispose()
        {
            Arrays.Dispose();
        }

        Task<MeshNativeArrays> t;
        int? tskId;
        Action<int> tskEnd;
    }

    public class PLYUtils
    {
        public static Task<MeshArrays> CreateArraysFromPlyAsync( string ply, bool generateNormals, bool showWarnings, bool doUnsafe )
        {
            return new Task<MeshArrays>( () => CreateArraysFromPly( ply, generateNormals, showWarnings, doUnsafe ) );
        }


        // Reconstruct the faces with (hard edges) as many models have shown to have inverted normals due to
        // swapping winding order.
        public static MeshArrays CreateArraysFromPly( string ply, bool generateNormals, bool showWarnings, bool doUnsafe )
        {
            var rc = PLYParser.LoadFromPly( ply, showWarnings, doUnsafe);
            bool hasNormals = rc.HasNormals();
            bool hasColors  = rc.HasColors();
            bool hasAlpha   = rc.HasAlpha();
            Vector3 [] vertices = new Vector3[rc.numFaces*3];
            Vector3 [] normals = hasNormals ? new Vector3[rc.numFaces*3] : null;
            Color   [] colors  = hasColors ? new Color[rc.numFaces*3] : null;
            int [] indices = new int[rc.numFaces*3];
            for ( int f = 0;f < rc.numFaces;f++ )
            {
                int f3 = f*3;
                for ( int i = 0;i < 3;i++ )
                {
                    int vi = rc.indices[f3+i];
                    int fi = vi * rc.numVertexAttributes;
                    vertices[f3+i] = new Vector3(
                                        (float)rc.vertexData[fi + rc.positionOffset.x],
                                        (float)rc.vertexData[fi + rc.positionOffset.z],
                                        (float)rc.vertexData[fi + rc.positionOffset.y]
                                        );
                    if ( hasNormals && !generateNormals )
                    {
                        normals[f3+i] = new Vector3(
                            (float)rc.vertexData[fi + rc.normalOffset.x],
                            (float)rc.vertexData[fi + rc.normalOffset.z],
                            (float)rc.vertexData[fi + rc.normalOffset.y]
                            );
                    }
                    if ( hasColors )
                    {
                        colors[i] = new Color(
                            (float)rc.vertexData[fi + rc.colorOffset.x],
                            (float)rc.vertexData[fi + rc.colorOffset.z],
                            (float)rc.vertexData[fi + rc.colorOffset.y],
                            hasAlpha ? (float)rc.vertexData[fi + rc.alphaOffset] : 1.0f
                            );
                    }
                }
                indices[f3+0] = f3+2;
                indices[f3+1] = f3+1;
                indices[f3+2] = f3+0;
            }
            MeshArrays arr = new MeshArrays();
            arr.vertices   = vertices;
            arr.indices    = indices;
            arr.colors     = colors;
            arr.normals    = normals;
            if ( generateNormals )
            {
                GenerateHardNormals( ref arr, showWarnings );
            }
            return arr;
        }

        public static MeshNativeArrays CreateNativeArraysFromPly( string ply, bool showWarnings, bool doUnsafe )
        {
            var rc = PLYParser.LoadFromPly( ply, showWarnings, doUnsafe);
            bool hasColors  = rc.HasColors();
            bool hasAlpha   = rc.HasAlpha();
            NativeArray<MeshNativeArrays.Vertex> vertices = new NativeArray<MeshNativeArrays.Vertex>( rc.numFaces*3, Allocator.Persistent, NativeArrayOptions.UninitializedMemory );
            NativeArray<int> indices = new NativeArray<int>( rc.numFaces*3, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);
            Vector3 [] tempVec = new Vector3[3];
            Color [] tempCol = new Color[3];
            MeshNativeArrays.Vertex v0 = new MeshNativeArrays.Vertex();
            MeshNativeArrays.Vertex v1 = new MeshNativeArrays.Vertex();
            MeshNativeArrays.Vertex v2 = new MeshNativeArrays.Vertex();
            for ( int f = 0;f < rc.numFaces;f++ )
            {
                int f3 = f*3;
                indices[f3+0] = f3+2;
                indices[f3+1] = f3+1;
                indices[f3+2] = f3+0;
                for ( int i = 0;i < 3;i++ )
                {
                    int vi = rc.indices[f3+i];
                    int fi = vi * rc.numVertexAttributes;
                    tempVec[i].x = (float)rc.vertexData[fi + rc.positionOffset.x];
                    tempVec[i].y = (float)rc.vertexData[fi + rc.positionOffset.z];
                    tempVec[i].z = (float)rc.vertexData[fi + rc.positionOffset.y];
                    if ( hasColors )
                    {
                        tempCol[i].r = (float)rc.vertexData[fi + rc.colorOffset.x];
                        tempCol[i].g = (float)rc.vertexData[fi + rc.colorOffset.z];
                        tempCol[i].b = (float)rc.vertexData[fi + rc.colorOffset.y];
                        tempCol[i].a = hasAlpha ? (float)rc.vertexData[fi + rc.alphaOffset] : 1.0f;
                    }
                }
                var e1 = tempVec[1] - tempVec[0];
                var e2 = tempVec[2] - tempVec[0];
                var n  = -Vector3.Cross( e1, e2 ).normalize2();
                v0.pos = tempVec[0];
                v1.pos = tempVec[1];
                v2.pos = tempVec[2];
                v0.nor = n;
                v1.nor = n;
                v2.nor = n;
                v0.col = tempCol[0];
                v1.col = tempCol[1];
                v2.col = tempCol[2];
                vertices[f3+0] = v0;
                vertices[f3+1] = v1;
                vertices[f3+2] = v2;
            }
            MeshNativeArrays arr = new MeshNativeArrays();
            arr.vertices = vertices;
            arr.indices = indices;
            return arr;
        }

        private static void GenerateHardNormals( ref MeshArrays arr, bool showWarnings )
        {
            if ( arr.indices != null && arr.indices.Length != 0 )
            {
                arr.normals = new Vector3[arr.vertices.Length];
                for ( int i = 0;i < arr.indices.Length/3;i++ )
                {
                    int i0 = arr.indices[i*3+0];
                    int i1 = arr.indices[i*3+1];
                    int i2 = arr.indices[i*3+2];
                    var v0 = arr.vertices[i0];
                    var v1 = arr.vertices[i1];
                    var v2 = arr.vertices[i2];
                    var e1 = v1-v0;
                    var e2 = v2-v0;
                    var n  = Vector3.Cross( e1, e2 ).normalize2();
                    arr.normals[i0] = n;
                    arr.normals[i1] = n;
                    arr.normals[i2] = n;
                }
            }
            else if ( showWarnings )
            {
                UnityEngine.Debug.LogWarning( "Wanted to generate hard normals but not indices found." );
            }
        }

        private static void GenerateSoftNormals( ref MeshArrays arr, bool showWarnings )
        {
            if ( arr.indices != null )
            {
                arr.normals = new Vector3[arr.vertices.Length];
                for ( int i = 0;i < arr.indices.Length/3;i++ )
                {
                    int i0 = arr.indices[i*3+0];
                    int i1 = arr.indices[i*3+1];
                    int i2 = arr.indices[i*3+2];
                    var v0 = arr.vertices[i0];
                    var v1 = arr.vertices[i1];
                    var v2 = arr.vertices[i2];
                    var e1 = v1-v0;
                    var e2 = v2-v0;
                    var n  = Vector3.Cross( e1, e2 ).normalize2();
                    arr.normals[i0] += n;
                    arr.normals[i1] += n;
                    arr.normals[i2] += n;
                }
                for ( int i = 0;i<arr.normals.Length;i++ )
                    arr.normals[i] = arr.normals[i].normalize2();
            }
            else if ( showWarnings )
            {
                UnityEngine.Debug.LogWarning( "Wanted to generate soft normals but not indices found." );
            }
        }

        public static Mesh CreateMeshFromPly( string ply, bool recalculateNormals, bool showWarnings, bool doUnsafe )
        {
            var arr = CreateArraysFromPly( ply, recalculateNormals, showWarnings, doUnsafe );
            return arr.ToMesh();
        }

        public static void CreatePointCacheFromPly( string ply, bool asBinary, bool showWarnings = true, bool doUnsafe = true )
        {
            // Harveste required data from ply
            var rc = PLYParser.LoadFromPly( ply, showWarnings, doUnsafe );
            bool hasColors  = rc.HasColors();
            bool hasNormals = rc.HasNormals();
            bool hasAlpha   = rc.HasAlpha();
            Vector3 [] vertices = new Vector3[rc.numVertices];
            Vector4 [] colors   = hasColors ? new Vector4[rc.numVertices] : null;
            Vector3 [] normals  = hasNormals ? new Vector3[rc.numVertices] : null;
            for ( int i = 0;i < rc.numVertices;i++ )
            {
                int offset = i * rc.numVertexAttributes;
                vertices[i].x = (float)rc.vertexData[offset + rc.positionOffset.x];
                vertices[i].z = (float)rc.vertexData[offset + rc.positionOffset.y];
                vertices[i].y = (float)rc.vertexData[offset + rc.positionOffset.z];
                if ( hasNormals )
                {
                    normals[i].x = (float)rc.vertexData[offset + rc.normalOffset.x];
                    normals[i].z = (float)rc.vertexData[offset + rc.normalOffset.y];
                    normals[i].y = (float)rc.vertexData[offset + rc.normalOffset.z];
                }
                if ( hasColors )
                {
                    colors[i].x = (float)rc.vertexData[offset + rc.colorOffset.x];
                    colors[i].y = (float)rc.vertexData[offset + rc.colorOffset.y];
                    colors[i].z = (float)rc.vertexData[offset + rc.colorOffset.z];
                    colors[i].w = 1;
                }
                if ( hasAlpha )
                {
                    colors[i].w = (float)rc.vertexData[offset + rc.alphaOffset];
                }
            }
            string output = Path.ChangeExtension(ply, ".pcache");
            PCache2 pcache = new PCache2();
            pcache.AddVector3Property( "position" );
            pcache.SetVector3Data( "position", vertices.ToList() );
            if ( hasNormals )
            {
                pcache.AddVector3Property( "normal" );
                pcache.SetVector3Data( "normal", normals.ToList() );
            }
            if ( hasColors )
            {
                pcache.AddColorProperty( "color" );
                pcache.SetColorData( "color", colors.ToList() );
            }
            pcache.SaveToFile( output, asBinary ? PCache2.Format.Binary : PCache2.Format.Ascii );
        }

        public static void CreatePlyFromPointCache( string cacheFile, bool binary )
        {
            PCache2 pcache = PCache2.FromFile( cacheFile );
            // PCache is almost same as ply, so we remap the names, this should suffice.
            static string pcacheNameToPlyName( string name )
            {
                return name switch
                {
                    "position.x" => "x",
                    "position.y" => "y",
                    "position.z" => "z",
                    "normal.x" => "nx",
                    "normal.y" => "ny",
                    "normal.z" => "nz",
                    "color.r" => "red",
                    "color.g" => "green",
                    "color.b" => "blue",
                    "color.a" => "apha",
                    _ => throw new InvalidOperationException( "Unexpected pcache name" ),
                };
            }
            string output = Path.ChangeExtension(cacheFile, "ply");
            pcache.SaveToFile( output, binary ? PCache2.Format.Binary : PCache2.Format.Ascii, pcacheNameToPlyName );
        }
    }
}