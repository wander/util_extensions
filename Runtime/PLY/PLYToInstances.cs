using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Wander
{
    public class PLYToInstances : MonoBehaviour
    {
        public string plyFile = "";
        public Material material;
        public Mesh mesh;
        public float scale = 0.1f;
        public int limit = -1;
        public bool generateNormals = true;
        public bool showWarnings = true;
        public bool doUnsafe = true;

        public void Generate()
        {
            MeshArrays arr = PLYUtils.CreateArraysFromPly(plyFile, generateNormals, showWarnings, doUnsafe);
            Vector4 [] positions = new Vector4[arr.vertices.Length];
            for ( int i = 0; i < positions.Length; i++ )
            {
                positions[i] = new Vector4( arr.vertices[i].x, arr.vertices[i].y, arr.vertices[i].z, scale );
                positions[i] *= 10;
            }
            GameObject go = new GameObject("PlyFile");
            var ri = go.AddComponent<RenderInstanced>();
            ri.positions = positions;
            ri.limit = limit;
            ri.material = material;
            ri.mesh = mesh;
            ri.submeshIndex = 0;
        }
    }

#if UNITY_EDITOR

    [CustomEditor( typeof( PLYToInstances ) )]
    [InitializeOnLoad]
    public class PlyCreateCloudEditor : Editor
    {
        static PlyCreateCloudEditor()
        {

        }

        public override void OnInspectorGUI()
        {
            PLYToInstances settings = (PLYToInstances)target;

            DrawDefaultInspector();

            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Select ply file" ) )
                {
                    settings.plyFile = EditorUtility.OpenFilePanel( settings.plyFile, "", "ply" );
                }
            }
            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Generate" ) )
                {
                    if ( !string.IsNullOrWhiteSpace( settings.plyFile ) )
                    {
                        settings.Generate();
                    }
                }
            }
            GUILayout.EndHorizontal();
        }
    }
#endif

}