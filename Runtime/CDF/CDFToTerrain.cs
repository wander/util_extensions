using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Wander
{
    public class CDFToTerrain : MonoBehaviour
    {
        public string heightmap      = "dem.nc";
        public int    resolution     = 1025;
        public Vector3 size          = new Vector3(384, 160, 256);
        public float  postScale      = 1;
        public string keyElevation   = "dem";
        public Material defaultMat;

        public void Generate()
        {
            float [,] heights = NetCDFUtils.CreateTerrain( heightmap, resolution, keyElevation, -500, 500 );
            CreateGameObject( heights );
        }

        void CreateGameObject( float[,] heights )
        {
            TerrainData td = new TerrainData();
            //   td.baseMapResolution = 1000;
            td.size = size;
            td.heightmapResolution = resolution;
            td.SetHeights( 0, 0, heights );

            GameObject terrainGo = new GameObject("CFDTerrain");
            Terrain ter          = terrainGo.AddComponent<Terrain>();
            TerrainCollider tc   = terrainGo.AddComponent<TerrainCollider>();
            ter.terrainData      = td;
            tc.terrainData       = td;

            if ( defaultMat != null )
                ter.materialTemplate = defaultMat;
            else
                ter.materialTemplate = Resources.Load<Material>( "DefaultCFDTerrainMat" );
        }
    }

#if UNITY_EDITOR

    [CustomEditor( typeof( CDFToTerrain ) )]
    [InitializeOnLoad]
    public class ReadCFDEditor : Editor
    {
        static ReadCFDEditor()
        {

        }

        public override void OnInspectorGUI()
        {
            CDFToTerrain settings = (CDFToTerrain)target;

            DrawDefaultInspector();

            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Select heightmap" ) )
                {
                    settings.heightmap = EditorUtility.OpenFilePanel( settings.heightmap, "", "nc" );
                }
            }
            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button( "Generate" ) )
                {
                    if ( !string.IsNullOrWhiteSpace( settings.heightmap ) )
                    {
                        settings.Generate();
                    }
                }
            }
            GUILayout.EndHorizontal();
        }
    }
#endif

}