using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Wander
{
    /* Helper script to render instanced. */
    [ExecuteAlways()]
    public class RenderInstanced : MonoBehaviour
    {
        public Vector4 [] positions;
        public int limit = -1;
        public Material material;
        public Mesh mesh;
        public int submeshIndex = 0;
        public ShadowCastingMode shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        public bool receiveShadows = true;
        
        // For caching
        int oldLimit = -2;
        int oldSubmeshIndex = -1;
        Mesh oldMesh;
        Bounds bounds;

        // GPU data
        List<ComputeBuffer> positionBuffers;
        List<ComputeBuffer> argBuffers;

        void RebuildPositionsBuffer()
        {
            bounds = new Bounds();
            if ( positionBuffers == null )
            {
                positionBuffers = new List<ComputeBuffer>();
            }
            else
            {
                positionBuffers.ForEach( pb => pb.Release() );
                positionBuffers.Clear();
            }
            int count  = limit == -1 ? positions.Length : Mathf.Min(limit, positions.Length);
            int offset = 0;
            while ( count != 0 )
            {
                int numInstances = Mathf.Min( 1023, count );
                ComputeBuffer positionBuffer = new ComputeBuffer(numInstances, 16, ComputeBufferType.Default);
                positionBuffer.SetData( positions, offset, 0, numInstances );
                positionBuffers.Add( positionBuffer );
                for ( int i = 0; i < numInstances; i++ )
                    bounds.Encapsulate( positions[offset+i] );
                count  -= numInstances;
                offset += numInstances;
            }
            oldLimit = limit;
        }

        void RebuildArgsBuffer()
        {
            if ( argBuffers == null )
            {
                argBuffers = new List<ComputeBuffer>();
            }
            else
            {
                argBuffers.ForEach( cb => cb.Release() );
                argBuffers.Clear();
            }
            submeshIndex = Mathf.Clamp( submeshIndex, 0, mesh.subMeshCount-1 );
            positionBuffers.ForEach( pb => 
            {
                uint [] args = new uint[5];
                ComputeBuffer indirectArgs = new ComputeBuffer( 1, args.Length*sizeof(uint), ComputeBufferType.IndirectArguments );
                args[0] = mesh.GetIndexCount( submeshIndex );
                args[1] = (uint)pb.count;
                args[2] = mesh.GetIndexStart( submeshIndex );
                args[3] = mesh.GetBaseVertex( submeshIndex );
                args[4] = 0;
                indirectArgs.SetData( args );
                argBuffers.Add( indirectArgs );
            } );
            oldSubmeshIndex = submeshIndex;
            oldMesh = mesh;
        }

        private void Update()
        {
            if ( mesh == null || material == null )
                return;
            if ( oldMesh != mesh || submeshIndex != oldSubmeshIndex || limit != oldLimit )
            {
                RebuildPositionsBuffer();
                RebuildArgsBuffer();
            }
            if ( positionBuffers == null || positionBuffers.Count == 0  || argBuffers == null || argBuffers.Count == 0 )
                return;

            for ( int i = 0; i < positionBuffers.Count; i++ )
            {
                MaterialPropertyBlock pb = new MaterialPropertyBlock();
                pb.SetBuffer( "positionBuffer", positionBuffers[i] );
                material.SetBuffer( "positionBuffer", positionBuffers[i] );
                Graphics.DrawMeshInstancedIndirect( mesh, submeshIndex, material, new Bounds(Vector3.zero, Vector3.one*10000), argBuffers[i], 0, pb, shadowCastingMode, receiveShadows );
            //    Graphics.DrawMeshInstanced( mesh, submeshIndex, material, matrixArrays[i], matrixArrays[i].Length, new MaterialPropertyBlock(), shadowCastingMode, receiveShadows );
            }
        }

        private void OnDestroy()
        {
            if ( positionBuffers != null ) positionBuffers.ForEach( pb => pb.Release() );
            if ( argBuffers != null ) argBuffers.ForEach( ab => ab.Release() );
        }
    }

}