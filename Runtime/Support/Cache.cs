﻿using System.IO;
using UnityEngine;

namespace Wander
{
    public class Cache
    {
        static string _cacheName = "Cache";

        public static string CacheFolderName => _cacheName;

        public static string CacheFolder
        {
            get
            {
                CreateCacheFolder();
                return _cacheName;
            }
        }

        public static void CreateCacheFolder()
        {
            var path = Path.Combine( Application.streamingAssetsPath, _cacheName );
            if ( !Directory.Exists( path ) )
                Directory.CreateDirectory( path );
        }

        public static string GetCacheName( string fileIn )
        {
            var relPath = Path.GetRelativePath( Application.streamingAssetsPath, fileIn );
            relPath = relPath.Replace( "\\", "/" );
            relPath = relPath.Replace( Path.GetExtension( relPath ), "" );
            var relCachePath = Path.Combine( CacheFolder, relPath );
            relCachePath = relCachePath.Replace( "\\", "/" );
            var absCachePath = Path.Combine( Application.streamingAssetsPath, relCachePath );
            var res = absCachePath.Replace( "\\", "/" );
            return res;
        }
    }
}